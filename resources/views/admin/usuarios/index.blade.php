@extends('layouts.admin')

@section('titulo','Área Administrativa')

@section('conteudo')

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                    Tabela
                <a href="#" class="btn btn-danger float-right">
                    <i class="fas fa-plus"></i> Adicionar
                </a>

                </h2>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-12">
               <table class="table table-bordered table-striped">
                   <thead>
                       <tr>
                           <th>#</th>
                           <th>Nome</th>
                           <th>status</th>
                           <th>Qtde</th>
                           <th>Noticias</th>
                           <th>Data Cadastro</th>
                           <th>Ação</th>
                       </tr>
                   </thead>
                   <tbody>
                       <tr>
                           <td scope="row">1</td>
                           <td>Erick Mathias</td>
                           <td>Fazendo nada</td>
                           <td>lllllllllllllll</td>
                           <td>KKKKKKKKKKKKKKKKK</td>
                           <td>14/05/2019 22:18</td>
                           <td>XXXXXXXXXXXXXXXX</td>
                       </tr>
                       
                   </tbody>
               </table>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <nav aria-label="Page navigation">
                  <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>

        <li class="page-item active"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>

                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
            </div>
        </div>
    </div>


@endsection