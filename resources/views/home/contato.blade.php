@extends('layouts.app')
@section('titulo','Contato')

@section('conteudo')
    
    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                <h2>Contato</h2>
                <p>Faça sua sugestão, criticas ou tire suas duvidas sobre algum conteudo publicado</p>
            </div>            
        </div>
        <div class="row">
            <div class="col-8 mx-auto">
                <form action="#" method="post">
                    <div class="form-group">
                        <label for="nome">Nome</label>
                        <input type="text" id="nome" name="nome" placeholder="Digite o nome" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" id="email" name="email" placeholder="Digite o E-mail" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="telefone">Telefone</label>
                        <input type="text" id="telefone" name="telefone" placeholder="(00) 00000 0000" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="assunto">Assunto</label>
                        <input type="text" id="assunto" name="assunto" placeholder="Digite o Assunto" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="mensagem">Mensagem</label>
                        <textarea id="mensagem" class="form-control" name="mensagem" placeholder="Digite a mensagem"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Enviar</button>
                    </div>
                </form>
            </div>
        </div>

    </div>


@endsection

