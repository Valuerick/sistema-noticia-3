@extends('layouts.app')

@section('titulo','Tecnologia')

@section('conteudo')

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <h2>Tecnologia</h2>
            </div>
        </div>

        <div class="row">

                @for($i = 1; $i <= 3; $i++) 
                
                 <div class="col-md-4 mt-5">
                    <article class="card">
                        <a href="#">
                            <img class="img-fluid" src="https://via.placeholder.com/500x250">
                        </a>
        
                        <div class="card-body">
                            <h2 class="card-title">
                                <a href="#">Titulo Noticia</a>
                            </h2>
                            <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Harum ad praesentium
                                quibusdam facilis modi, voluptates tempore sunt commodi, culpa qui quia magni odit adipisci non
                                repellendus. Ex corporis sapiente delectus.</p>
                        </div>
                        <div class="card-footer">
                            30/04/2019
                        </div>
                    </article>
                </div>
        
            @endfor
        
        </div>


    </div>


@endsection